package pentogame.views.renderers;

import pentogame.objects.PentoObject;

public interface ObjectRenderer {

	public void render(PentoObject object);
}
