# The InproTK+DialogOS Pentomino game!

Software to run a puzzle game to build an elephant (or potentially anything else) 
out of the famous Pentomino pieces using voice control.

## Setup description:
* buy DialogOS (this is probably the slowest part in the process)
* clone the git
* get InproTK in a version from some time in 2011 (TODO: find git-ID and/or provide a full jar)
* open up eclipse, start SimpleReco in the given configuration, the pento server should start.
* only then start DialogOS, load dialogos.txt and start up
* ideally, DialogOS will be able to connect to the running pento server

## People:

Students: Radu Comaneci (now at Yelp), Maike Paetzel (now at Uppsala University), Mircea Pricop (now at Google), Philipp Schlesinger (still in Hamburg)  
Supervisors: Wolfgang Menzel, Timo Baumann

see also: 

* the published paper: http://nbn-resolving.org/urn:nbn:de:gbv:18-228-7-1836
* a video: https://www.youtube.com/watch?v=XgLod83BRSk
* the original student project: https://nats-www.informatik.uni-hamburg.de/ProSDS1112/WebHome


